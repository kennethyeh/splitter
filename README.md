# splitter #

## Build & Run ##

```sh
$ sbt
> jetty:start
```

## Endpoints ##
### POST split ###
Use this endpoint to evaluate a list of rules against a provided user's attributes.

Requests are expected to be HTTP POST with a JSON content body, and `Content-Type application/json; charset=UTF-8`

#### Expected Fields ####
The request body is expected to be an object with the following fields:

| Field Name    | Type          | Description  |
| ------------- |:-------------:|-------------:|
| userAttributes| object       | a map of a user's attributeNames to its values |
| rules         | string array | a list of rules to evaluate against|

#### Example Request ####

```
POST localhost:8080/split
content-type:application/json
{
  "userAttributes":{
    "gender":"male",
    "height":170,
    "location":{
      "city":"San Francisco",
      "state":"CA"
    },
    "hometown":{
      "city":"Rockville",
      "state":"MD"
    }
  },
  "rules":[
    "gender",
    "height",
    "region",
    "travel"
  ]
}
```
#### Example Response ####
```
{
  "gender":[
    "binaryGendered"
  ],
  "height":[
    "mediumHeight"
  ],
  "region":[
    "pacific",
    "west"
  ],
  "travel":[
    "outOfTown"
  ]
}
```

### GET rules ###
Use this endpoint to get a list of all the rules that the service can evaluate
#### Example Request ####
```
GET localhost:8080/rules
```
#### Example Response ####
```
[
  "gender",
  "height",
  "region",
  "travel"
]
```

## Design Considerations ##

### JSON Protocol ###

I opted to use this JSON protocol over query parameters and headers to allow for more complex attributes(nested objects) to be sent more cleanly.

I purposefully left the JSON request format as open as possible. This leaves the data transfer layer very simple and pushes the logic of validation and evaluation of the data into the rules logic, where it would be anyway.

### Statelessness ###
For the purpose of this exercise, I decided not to maintain state of any of the processes that this service provides. Arguably, the only state that is kept is the hard coded rules.

I understand it isn't entirely practical in a production setting as it is very common to build complex rules based on some state(for example, dividing users into control/experiment spec evenly). However, I do believe the RulesEvaluator framework can be extended to take more sophisticated rule functions that do maintain state without much extra work.
