package com.ikenfly.splitter

import org.json4s.{DefaultFormats, Formats}
import org.json4s.jackson.Serialization
import org.scalatra.test.scalatest._
import com.ikenfly.splitter.rules.RuleEvaluator.{GENDER_SPLIT_KEY, HEIGHT_SPLIT_KEY, REGION_SPLIT_KEY, TRAVEL_SPLIT_KEY}

/**
  * Integration tests to check that the servlet is able to use the RuleEvaluator correctly
  */
class SplitterTests extends ScalatraFunSuite {

  addServlet(classOf[Splitter], "/*")
  implicit val formats : Formats = DefaultFormats

  test("POST / with valid data should return 200 and a result for all rules") {
    val requestBody =
      Serialization.write(
        Map("userAttributes" ->
            Map("gender" -> "male",
                "height" -> 178,
                "location" -> Map("city" -> "San Francisco",
                                  "state" -> "CA"),
                "hometown" -> Map("city" -> "Rockville",
                                  "state" -> "MD")),
            "rules" -> List(GENDER_SPLIT_KEY, HEIGHT_SPLIT_KEY, REGION_SPLIT_KEY, TRAVEL_SPLIT_KEY))
      )

    submit(method = "POST", path = "/split", headers = Map("content-type" -> "application/json"), body = requestBody.getBytes) {
      status should equal (200)
      List(GENDER_SPLIT_KEY, HEIGHT_SPLIT_KEY, REGION_SPLIT_KEY, TRAVEL_SPLIT_KEY).foreach(ruleName => assert(body.contains(ruleName)))
    }
  }

  test("POST / without valid data for a rule should return 200 and no tags for that rule") {
    val requestBody =
      Serialization.write(
        Map("userAttributes" ->
          Map("gender" -> "male",
            "height" -> "banana",
            "location" -> Map("city" -> "San Francisco",
              "state" -> "CA")),
          "rules" -> List(GENDER_SPLIT_KEY, HEIGHT_SPLIT_KEY, REGION_SPLIT_KEY, TRAVEL_SPLIT_KEY))
      )

    submit(method = "POST", path = "/split", headers = Map("content-type" -> "application/json"), body = requestBody.getBytes) {
      status should equal (200)
      List(GENDER_SPLIT_KEY, REGION_SPLIT_KEY).foreach(ruleName => assert(body.contains(ruleName)))
      assert(!body.contains(HEIGHT_SPLIT_KEY))
      assert(!body.contains(TRAVEL_SPLIT_KEY))
    }
  }

  test("Post / with malformed data should 500") {
    submit(method = "POST", path = "/split", headers = Map("content-type" -> "application/json"), body = "notjson") {
      status should equal(500)
    }
  }

  test("Post / without content-type header should 500") {
    submit(method = "POST", path = "/split", body = "notjson") {
      status should equal(500)
    }
  }
}
