package com.ikenfly.splitter.rules

import org.scalatest.FunSuite
import com.ikenfly.splitter.rules.RuleEvaluator.{evaluate, GENDER_SPLIT_KEY, HEIGHT_SPLIT_KEY, REGION_SPLIT_KEY, TRAVEL_SPLIT_KEY}

/**
  * Unit Testing for the rule logic
  */
class RuleEvaluatorTest extends FunSuite {

  test("Test Gender Rule") {
    assert(evaluate(GENDER_SPLIT_KEY, Map("gender" -> "male")).contains(List("binaryGendered")))
    assert(evaluate(GENDER_SPLIT_KEY, Map("gender" -> "MALE")).contains(List("binaryGendered")))
    assert(evaluate(GENDER_SPLIT_KEY, Map("gender" -> "Genderfluid")).contains(List("nonbinaryGendered")))
    assert(evaluate(GENDER_SPLIT_KEY, Map("gender" -> 1)).isEmpty)
    assert(evaluate(GENDER_SPLIT_KEY, Map.empty).isEmpty)
  }

  test("Test Height Rule") {
    assert(evaluate(HEIGHT_SPLIT_KEY, Map("height" -> 188)).contains(List("tallHeight")))
    assert(evaluate(HEIGHT_SPLIT_KEY, Map("height" -> 160)).contains(List("mediumHeight")))
    assert(evaluate(HEIGHT_SPLIT_KEY, Map("height" -> 179.88)).contains(List("mediumHeight")))
    assert(evaluate(HEIGHT_SPLIT_KEY, Map("height" -> 140)).contains(List("shortHeight")))
    assert(evaluate(HEIGHT_SPLIT_KEY, Map("height" -> "5 ft 8 in.")).isEmpty)
    assert(evaluate(HEIGHT_SPLIT_KEY, Map.empty).isEmpty)
  }

  test("Test Region Rule") {
    def inputGenerator(state: String) = { Map("location" -> Map("state" -> state))}
    assert(evaluate(REGION_SPLIT_KEY, inputGenerator("CA")).contains(List("pacific", "west")))
    assert(evaluate(REGION_SPLIT_KEY, inputGenerator("OT")).contains(List("nonUnitedStates")))
    assert(evaluate(REGION_SPLIT_KEY, Map("location" -> Map("city" -> "San Francisco"))).isEmpty)
    assert(evaluate(REGION_SPLIT_KEY, Map.empty).isEmpty)
  }

  test("Test Travel Rule") {
    assert(evaluate(TRAVEL_SPLIT_KEY, Map("hometown" -> Map("state" -> "MD"), "location" -> Map("state" -> "CA"))).contains(List("outOfTown")))
    assert(evaluate(TRAVEL_SPLIT_KEY, Map("hometown" -> Map("state" -> "CA"), "location" -> Map("state" -> "CA"))).contains(List("inHometown")))
    assert(evaluate(TRAVEL_SPLIT_KEY, Map("location" -> "California")).isEmpty)
    assert(evaluate(TRAVEL_SPLIT_KEY, Map("hometown" -> "California")).isEmpty)
    assert(evaluate(TRAVEL_SPLIT_KEY, Map("location" -> Map("state" -> "CA"))).isEmpty)
    assert(evaluate(TRAVEL_SPLIT_KEY, Map("hometown" -> Map("state" -> "CA"))).isEmpty)
    assert(evaluate(TRAVEL_SPLIT_KEY, Map.empty).isEmpty)
  }
}
