package com.ikenfly.splitter.models

/**
  * Data model for an incoming request to split/tag a user
  *
  * @param userAttributes json dictionary mapping a user's attributeNames -> values
  * @param rules list of rule names which the service will evaluate
  */
case class SplitRequest(userAttributes: Map[String, Any], rules: List[String])