package com.ikenfly.splitter

import com.ikenfly.splitter.models.SplitRequest
import com.ikenfly.splitter.rules.RuleEvaluator.{evaluate, getRules}
import org.json4s.jackson.Serialization
import org.json4s.{DefaultFormats, Formats}
import org.scalatra._
import org.scalatra.json.JacksonJsonSupport


class Splitter extends ScalatraServlet with JacksonJsonSupport {

  protected implicit lazy val jsonFormats: Formats = DefaultFormats.withBigDecimal

  /**
    * Endpoint to split a user's data.
    * This endpoint expects a json payload that follows SplitRequest case class.
    */
  post("/split") {

    val tagRequest = parsedBody.extract[SplitRequest]
    val splits = tagRequest.rules.foldLeft(Map.empty[String, List[String]]) {
      (acc: Map[String, List[String]], ruleName: String) =>
        evaluate(ruleName, tagRequest.userAttributes) match {
          case Some(tags) => acc + (ruleName -> tags)
          case None => acc
        }
    }

    Ok(splits)
  }

  /**
    * Endpoint to expose the rules that the service can evaluate
    */
  get("/rules") {
    Ok(Serialization.write(getRules()))
  }

}
