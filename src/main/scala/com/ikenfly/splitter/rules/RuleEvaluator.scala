package com.ikenfly.splitter.rules

object RuleEvaluator {

  val GENDER_SPLIT_KEY = "gender"
  val HEIGHT_SPLIT_KEY = "height"
  val REGION_SPLIT_KEY = "region"
  val TRAVEL_SPLIT_KEY = "travel"

  /**
    * Rule based on a single string attribute
    */

  private val GENDER_SPLIT: Map[String, Any] => Option[List[String]] = (attributes: Map[String, Any]) => {
    attributes.get("gender") match {
      case Some(value: String) =>
        val binaryGenders = Set("male", "female")
        if (binaryGenders.contains(value.toLowerCase())) {
          Some(List("binaryGendered"))
        } else {
          Some(List("nonbinaryGendered"))
        }
      case _ => None
    }
  }

  /**
    * Rule based on a single numeric attribute
    */

  private val HEIGHT_SPLIT: Map[String, Any] => Option[List[String]] = (attributes: Map[String, Any]) => {
    attributes.get("height") match {
      case Some(value: Number) =>
        val shortThreshold = 156
        val tallThreshold = 180
        if (value.intValue() > tallThreshold) {
          Some(List("tallHeight"))
        } else if(value.intValue() < shortThreshold) {
          Some(List("shortHeight"))
        } else {
          Some(List("mediumHeight"))
        }
      case _ => None
    }
  }

  /**
    * Rule based on an attribute with a value as a nested object
    */

  private val REGION_SPLIT: Map[String, Any] => Option[List[String]] = (attributes: Map[String, Any]) => {
    attributes.get("location") match {
      case Some(locationValue: Map[String, Any]) =>
        locationValue.get("state") match {
          case Some(state: String) =>
            if (Set("CT", "ME", "MA", "NH", "RI", "VE").contains(state)) {
              Some(List("newEngland", "northEast"))
            } else if (Set("NJ", "NY", "PA").contains(state)) {
              Some(List("midAtlantic", "northEast"))
            } else if (Set("DE", "FL", "GA", "MD", "NC", "SC", "VA", "DC", "WV").contains(state)) {
              Some(List("southAtlantic", "south"))
            } else if (Set("AL", "KT", "MS", "TN").contains(state)) {
              Some(List("eastSouthCentral", "south"))
            } else if (Set("AR", "LA", "OK", "TX").contains(state)) {
              Some(List("westSouthCentral", "south"))
            } else if (Set("AZ", "CO", "ID", "MT", "NV", "NM", "WY").contains(state)) {
              Some(List("mountain", "west"))
            } else if (Set("AK", "CA", "HI", "OR", "WY").contains(state)) {
              Some(List("pacific", "west"))
            } else if (Set("IL", "ID", "MI", "WI").contains(state)) {
              Some(List("eastNorthCentral", "midwest"))
            } else if (Set("IA", "KS", "MN", "MO", "NE", "ND", "SD").contains(state)) {
              Some(List("westNorthCentral", "midwest"))
            } else {
              Some(List("nonUnitedStates"))
            }
          case _ => None
          }
      case _ => None
    }
  }

  /**
    * Rule based on multiple attributes
    */
  private val TRAVEL_SPLIT: Map[String, Any] => Option[List[String]] = (attributes: Map[String, Any]) => {
    (attributes.get("location"), attributes.get("hometown")) match {
      case (Some(location: Map[String, Any]), Some(hometown: Map[String, Any])) =>
        (location.get("state"), hometown.get("state")) match {
          case (Some(currentState: String), Some(homeState: String)) =>
            if (currentState == homeState) {
              Some(List("inHometown"))
            } else {
              Some(List("outOfTown"))
            }
          case (_, _) => None
        }
      case (_, _) => None
    }
  }

  private val STUB_RULE: Map[String, Any] => Option[List[String]] = (_: Map[String, Any]) => {
    None
  }

  private val rules: Map[String, Map[String, Any] => Option[List[String]]] =
    Map(GENDER_SPLIT_KEY -> GENDER_SPLIT,
      HEIGHT_SPLIT_KEY -> HEIGHT_SPLIT,
      REGION_SPLIT_KEY -> REGION_SPLIT,
      TRAVEL_SPLIT_KEY -> TRAVEL_SPLIT)

  def evaluate(ruleKey: String, attributes: Map[String, Any]) : Option[List[String]] = {
    rules.getOrElse(ruleKey, STUB_RULE)(attributes)
  }

  def getRules() = {
    rules.keySet
  }
}
